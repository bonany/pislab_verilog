# PIS LIB (Verilog Version)

Author: Dr. Bonan Yan

## Files Directory

- ```src/APIM.v```: analog PIM verilog behavioral model

- ```src/APIM_tb.v```: testbench for analog PIM verilog behavioral model

- ```src/DPIM.v```: digital PIM verilog behavioral model

- ```src/DPIM_tb.v```: testbench for digital PIM verilog behavioral model


## To Run

1. Step 1:  
```
cd [project_folder]
```

2. Step 2: run make

```
make
```

some useful makefile instructions:
```make apim```: test APIM.v
```make dpim```: test APIM.v
```make plot```: plot the testing waveform